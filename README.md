
# Predict Sales Rossmann Stores

## 0. Challenge

You are provided with historical sales data for 1,115 Rossmann stores. The task is to forecast the "Sales" column for the test set. Note that some stores in the dataset were temporarily closed for refurbishment. [Link Kaggle](https://www.kaggle.com/c/rossmann-store-sales)

## 1. Business Problem

Rossmann operates over 3,000 drug stores in 7 European countries. Currently, Rossmann store managers are tasked with predicting their daily sales for up to six weeks in advance. Store sales are influenced by many factors, including promotions, competition, school and state holidays, seasonality, and locality. With thousands of individual managers predicting sales based on their unique circumstances, the accuracy of results can be quite varied.

## 2. Business Assumptions
- Some stores in the dataset were temporarily closed for refurbishment.

## 3. Solution Strategy

My strategy to solve this challenge was:

**Step 01. Data Description:** My goal is to use statistics metrics to identify data outside the scope of business.

**Step 02. Feature Engineering:** Derive new attributes based on the original variables to better describe the phenomenon that will be modeled.
![image](docs/DAYLY_STORE_SALES.png)

**Step 03. Data Filtering:** Filter rows and select columns that do not contain information for modeling or that do not match the scope of the business.

**Step 04. Exploratory Data Analysis:** Explore the data to find insights and better understand the impact of variables on model learning.
###### **Target variable distribution**
![image](docs/target_feature_distribution.png)

###### **Correlations of Numerical Variables**
![image](docs/num_features_corr.png)

**Step 05. Data Preparation:** Prepare the data so that the Machine Learning models can learn the specific behavior.

**Step 06. Feature Selection:** Selection of the most significant attributes for training the model.

**Step 07. Machine Learning Modelling:** Machine Learning model training

**Step 08. Hyperparameter Fine Tunning:** Choose the best values for each of the parameters of the model selected from the previous step.

**Step 09. Convert Model Performance to Business Values:** Convert the performance of the Machine Learning model into a business result.

**Step 10. Deploy Model to Production:** Publish the model in a cloud environment so that other people or services can use the results to improve the business decision.

## 4. Top 3 Data Insights

**Hypothesis 01:**  Stores with closer competitors should sell LESS
**False:** Stores with closest competitors sell MORE

**Hypothesis 02:** Stores with a larger assortment should sell more
**False.** Stores with a larger assortment sell LESS

**Hypothesis 03:** Stores open during the Christmas holiday should sell more
**False.** Stores opened during Christmas DON'T sell anymore

## 5. Machine Learning Model Applied

|Model Name	| MAE |	MAPE |	RMSE |
|:-------|:------|:-----|:-------|
|Linear Regression	|2073.27 +/- 257.04	|0.31 +/- 0.02|	2979.74 +/- 425.32|
|Lasso	|2108.05 +/- 282.6	|0.3 +/- 0.01|	3082.88 +/- 446.6|
|Random Forest Regressor|	1117.81 +/- 284.02|	0.15 +/- 0.03	|1708.85 +/- 431.47|
|XGBoost Regressor|	7012.39 +/- 563.38	|0.95 +/- 0.0|	7681.07 +/- 660.97|

##### Model choosed: **XGBoost Regressor for Tunning**

## 6. Machine Learning XGBoost Regressor(Tunning) Performance

![image](docs/xgboost_regressor.png)
## 7. Business Results
| Scenarios | Values |
|:---------|:-------|
|Predicitions| R$ 289,257,792.00|
|Worst scenario	|R$ 288,387,113.61|
|Best scenario	|R$ 290,128,419.86|

## 8. Conclusions

A Data Science project involves many different skills and techniques. In this project, it was clear the need to have a well-defined problem before starting the analyses. It was also observed that during a project several tests must be performed and that prior knowledge of the data is extremely important.

The Cyclic design methodology (CRISP-DM) offers a lot of security during design development. That's why it's important to keep in mind that the project must constantly evolve with periodic deliveries. As this is a project from start to finish, several difficulties were overcome until the model was implemented.

 I thank Meigarom (Comunidade DS) for the classes and for sharing a project solving method that can be replicated and serves as a basis for thinking about solutions to data science problems.

## 9. Lessons Learned

- Use a framework to solve Data Science problems;

- The importance of translating model results into business metrics;

- Be aware that the project developed must bring real value to the business;

- Techniques for analyzing and exploring data;

- Notions of regression problems and machine learning algorithms that can be used for this type of problem;

- The importance of cross-validations to confirm model results;

- Develop critical and analytical thinking for problem solving;

- Put a Machine Learning model into production via a Telegram bot;

- Understand and adopt the idea of ​​cyclical solutions (improving projects in each new cycle);

## 10. Next Steps to Improve

- Improve the results of the model metrics (compare with the results of notebooks in Kaggle);

- Test other types of Machine Learning algorithms;

- Publish the project to other clouds (AWS)

- Offer the user more information in the answer such as the current sales value and a graph;

- Organize the notebook to make it as clean as possible;
